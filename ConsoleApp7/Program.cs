﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp7
{
    class Program
    {
        static void Main(string[] args)
        {
            //注册一个消息提供器
            LocationTracker provider = new LocationTracker();


            //声明一个消息订阅器
            LocationReporter reporter1 = new LocationReporter("FixedGPS");

            //声明一个消息订阅器
            LocationReporter reporter2 = new LocationReporter("MobileGPS");

            //订阅操作
            reporter1.Subscribe(provider);
            reporter2.Subscribe(provider);

            //推送消息 此时reporter1与2进入OnNext函数
            provider.TrackLocation(new Location(47.6456, -122.1312));

            //reporter1注销订阅
            reporter1.Unsubscribe();

            Console.WriteLine("Reporter1 Unsubscribe");


            //推送消息，此时reporter2进入OnNext函数
            provider.TrackLocation(new Location(47.6677, -122.1199));

            //推送一个错误消息
            provider.TrackLocation(null);

            //正常结束消息提供器，所有订阅用户将进入OnCompleted函数
            provider.EndTransmission();


            Console.ReadLine();
            // The example displays output similar to the following:
            //      FixedGPS: The current location is 47.6456, -122.1312
            //      MobileGPS: The current location is 47.6456, -122.1312
            //      Reporter1 Unsubscribe
            //      MobileGPS: The current location is 47.6677, -122.1199
            //      MobileGPS: The location cannot be determined.
            //      The Location Tracker has completed transmitting data to MobileGPS.

        }
    }
}
